import 'package:flutter/material.dart';
import 'package:tomisha/colors.dart';
import 'package:tomisha/text_styles.dart';

class BannerMobileWidget extends StatelessWidget {
  final String imageLocation;
  final String title;
  const BannerMobileWidget({
    super.key,
    required this.imageLocation,
    required this.title,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Center(
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 20,
            ),
            child: Text(
              title,
              style: AppTextStyles.mediumStyle(
                fontSize: 42,
                color: AppColors.black,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Image.asset(imageLocation),
      ],
    );
  }
}
