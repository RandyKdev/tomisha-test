import 'package:flutter/material.dart';
import 'package:tomisha/breakpoints.dart';
import 'package:tomisha/models/tab_item.dart';
import 'package:tomisha/widgets/tab_item_desktop_widget.dart';
import 'package:tomisha/widgets/tab_item_mobile_widget.dart';

class TabBodyWidget extends StatelessWidget {
  final List<TabItem> items;
  const TabBodyWidget({super.key, required this.items});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.sizeOf(context);
    final isMobile = AppBreakpoints.isMobile(size.width);
    return Column(
      children: items
          .map(
            (e) => isMobile
                ? TabItemMobileWidget(item: e, index: items.indexOf(e))
                : TabItemDesktopWidget(item: e, index: items.indexOf(e)),
          )
          .toList(),
    );
  }
}
