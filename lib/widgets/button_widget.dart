import 'package:flutter/material.dart';
import 'package:tomisha/colors.dart';
import 'package:tomisha/text_styles.dart';

class ButtonWidget extends StatelessWidget {
  final double? width;
  final VoidCallback? onTap;
  final String title;
  const ButtonWidget({
    super.key,
    required this.title,
    this.width,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        height: 45,
        width: width,
        decoration: BoxDecoration(
          border: Border.all(color: Colors.white),
          gradient: const LinearGradient(
            colors: [AppColors.green, AppColors.blue],
            begin: FractionalOffset(0.0, 0.0),
            end: FractionalOffset(1.0, 0.0),
            tileMode: TileMode.clamp,
          ),
          borderRadius: BorderRadius.circular(10),
        ),
        margin: const EdgeInsets.symmetric(horizontal: 20),
        child: Center(
          child: Text(
            title,
            style: AppTextStyles.semiBoldStyle(
              color: AppColors.lightTeal,
            ),
          ),
        ),
      ),
    );
  }
}
