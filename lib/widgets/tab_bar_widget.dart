import 'package:flutter/material.dart';
import 'package:tomisha/colors.dart';
import 'package:tomisha/text_styles.dart';

class TabBarWidget extends StatelessWidget {
  final void Function(int) onSelect;
  final int currentIndex;
  final List<String> tabs;

  const TabBarWidget({
    super.key,
    required this.onSelect,
    required this.currentIndex,
    required this.tabs,
  });

  Widget _buildTabBarItem(String tab) {
    final index = tabs.indexOf(tab);
    final isCurrentIndex = currentIndex == index;
    final isFirst = index == 0;
    final isLast = index == tabs.length - 1;

    BorderRadius? borderRadius;
    EdgeInsets? margin;

    if (isFirst && isLast) {
      borderRadius = const BorderRadius.horizontal(
        left: Radius.circular(12),
        right: Radius.circular(12),
      );

      margin = const EdgeInsets.only(left: 20, right: 20);
    } else if (isFirst) {
      borderRadius = const BorderRadius.horizontal(
        left: Radius.circular(12),
      );

      margin = const EdgeInsets.only(left: 20);
    } else if (isLast) {
      borderRadius = const BorderRadius.horizontal(
        right: Radius.circular(12),
      );
      margin = const EdgeInsets.only(right: 20);
    }

    return Padding(
      padding: margin ?? EdgeInsets.zero,
      child: InkWell(
        onTap: () => onSelect(index),
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(
              width: 0,
              color: isCurrentIndex ? AppColors.teal : AppColors.greyishBlue,
            ),
            borderRadius: borderRadius,
            color: isCurrentIndex ? AppColors.teal : AppColors.white,
          ),
          padding: const EdgeInsets.symmetric(
            vertical: 10,
            horizontal: 20,
          ),
          width: 190,
          child: Center(
            child: Text(
              tab,
              style: AppTextStyles.semiBoldStyle(
                color: isCurrentIndex ? AppColors.lightTeal : AppColors.green,
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Container(
        margin: const EdgeInsets.symmetric(
          vertical: 20,
        ),
        child: Row(
          children: tabs.map((e) => _buildTabBarItem(e)).toList(),
        ),
      ),
    );
  }
}
