import 'package:flutter/material.dart';
import 'package:tomisha/colors.dart';
import 'package:tomisha/models/tab_item.dart';
import 'package:tomisha/text_styles.dart';

class TabItemMobileWidget extends StatelessWidget {
  final TabItem item;
  final int index;
  const TabItemMobileWidget({
    super.key,
    required this.item,
    required this.index,
  });

  Widget _buildTextView(double screenWidth) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Text(
          '${index + 1}.',
          style: AppTextStyles.regularStyle(
            fontSize: screenWidth * .3,
            color: AppColors.lightBlue,
          ),
          textAlign: TextAlign.center,
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 20,
            ),
            child: Text(
              item.title,
              style: AppTextStyles.mediumStyle(
                fontSize: screenWidth * .045,
              ),
              textAlign: TextAlign.left,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildImageView(double screenWidth) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 30,
      ),
      child: Image.asset(
        item.imageLocation,
        fit: BoxFit.contain,
        width: screenWidth * .7,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.sizeOf(context);
    final screenWidth = size.width;

    if (index == 0) {
      return SizedBox(
        height: screenWidth * .6,
        child: Stack(
          children: [
            Align(
              alignment: Alignment.topRight,
              child: _buildImageView(screenWidth),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: EdgeInsets.only(left: screenWidth * .05),
                child: _buildTextView(screenWidth),
              ),
            ),
          ],
        ),
      );
    }

    if (index == 1) {
      return ClipPath(
        clipBehavior: Clip.hardEdge,
        clipper: _CustomClipperTabItemDesktop(),
        child: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [AppColors.lightTeal, AppColors.bgBlue],
              begin: FractionalOffset(0.0, 0.0),
              end: FractionalOffset(1.0, 0.0),
              tileMode: TileMode.clamp,
            ),
          ),
          margin: const EdgeInsets.symmetric(vertical: 20),
          padding: const EdgeInsets.symmetric(vertical: 10),
          height: screenWidth * .85,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.bottomRight,
                child: _buildImageView(screenWidth),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Padding(
                  padding: EdgeInsets.only(left: screenWidth * .15),
                  child: _buildTextView(screenWidth),
                ),
              ),
            ],
          ),
        ),
      );
    }

    return SizedBox(
      height: screenWidth,
      child: Stack(
        children: [
          Align(
            alignment: Alignment.bottomCenter,
            child: _buildImageView(screenWidth),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Padding(
              padding: EdgeInsets.only(left: screenWidth * .25),
              child: _buildTextView(screenWidth),
            ),
          ),
        ],
      ),
    );
  }
}

class _CustomClipperTabItemDesktop extends CustomClipper<Path> {
  @override
  getClip(Size size) {
    var path = Path();
    var firstControlPoint = Offset(size.width * .05, 0);
    var firstEndPoint = Offset(size.width * .45, size.height * .15);
    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy,
        firstEndPoint.dx, firstEndPoint.dy);
    var secondControlPoint = Offset(size.width * .9, 0);
    var secondEndPoint = Offset(size.width, size.height * .05);
    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy,
        secondEndPoint.dx, secondEndPoint.dy);
    path.lineTo(size.width, size.height * .9);
    var thirdControlPoint = Offset(size.width * .7, size.height);
    var thirdEndPoint = Offset(size.width * .25, size.height * .9);
    path.quadraticBezierTo(thirdControlPoint.dx, thirdControlPoint.dy,
        thirdEndPoint.dx, thirdEndPoint.dy);
    var fourthControlPoint = Offset(size.width * .05, size.height);
    var fourthEndPoint = Offset(0, size.height);
    path.quadraticBezierTo(fourthControlPoint.dx, fourthControlPoint.dy,
        fourthEndPoint.dx, fourthEndPoint.dy);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper oldClipper) {
    return true;
  }
}
