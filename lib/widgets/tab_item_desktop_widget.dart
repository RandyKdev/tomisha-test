import 'package:flutter/material.dart';
import 'package:tomisha/colors.dart';
import 'package:tomisha/models/tab_item.dart';
import 'package:tomisha/text_styles.dart';

class TabItemDesktopWidget extends StatelessWidget {
  final TabItem item;
  final int index;
  const TabItemDesktopWidget({
    super.key,
    required this.item,
    required this.index,
  });

  Widget _buildTextView(double screenWidth) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Text(
          '${index + 1}.',
          style: AppTextStyles.regularStyle(
            fontSize: screenWidth * .1,
            color: AppColors.lightBlue,
          ),
          textAlign: TextAlign.center,
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 20,
            ),
            child: Text(
              item.title,
              style: AppTextStyles.mediumStyle(
                fontSize: screenWidth * .015,
              ),
              textAlign: TextAlign.left,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildImageView(double screenWidth) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 30,
      ),
      child: Image.asset(
        item.imageLocation,
        fit: BoxFit.contain,
        width: screenWidth * .3,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.sizeOf(context);
    final screenWidth = size.width;

    if (index == 0) {
      return SizedBox(
        width: screenWidth * .75,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(left: screenWidth * .05),
                child: _buildTextView(screenWidth),
              ),
            ),
            _buildImageView(screenWidth),
          ],
        ),
      );
    }

    if (index == 1) {
      return ClipPath(
        clipBehavior: Clip.hardEdge,
        clipper: _CustomClipperTabItemDesktop(),
        child: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [AppColors.lightTeal, AppColors.bgBlue],
              begin: FractionalOffset(0.0, 0.0),
              end: FractionalOffset(1.0, 0.0),
              tileMode: TileMode.clamp,
            ),
          ),
          margin: const EdgeInsets.symmetric(vertical: 20),
          child: Center(
            child: Container(
              margin: const EdgeInsets.symmetric(vertical: 20),
              width: screenWidth * .75,
              child: Row(
                children: [
                  SizedBox(width: screenWidth * .15),
                  _buildImageView(screenWidth),
                  Expanded(
                    child: _buildTextView(screenWidth),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    }

    return SizedBox(
      width: screenWidth * .75,
      child: Row(
        children: [
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: screenWidth * .15),
              child: _buildTextView(screenWidth),
            ),
          ),
          _buildImageView(screenWidth),
        ],
      ),
    );
  }
}

class _CustomClipperTabItemDesktop extends CustomClipper<Path> {
  @override
  getClip(Size size) {
    var path = Path();
    path.lineTo(0, size.height * .05);
    var firstControlPoint = Offset(size.width * .2, 0);
    var firstEndPoint = Offset(size.width * .45, size.height * .15);
    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy,
        firstEndPoint.dx, firstEndPoint.dy);
    var secondControlPoint = Offset(size.width * .8, 0);
    var secondEndPoint = Offset(size.width, size.height * .35);
    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy,
        secondEndPoint.dx, secondEndPoint.dy);
    path.lineTo(size.width, size.height * .65);
    var thirdControlPoint = Offset(size.width * .8, size.height);
    var thirdEndPoint = Offset(size.width * .45, size.height * .85);
    path.quadraticBezierTo(thirdControlPoint.dx, thirdControlPoint.dy,
        thirdEndPoint.dx, thirdEndPoint.dy);
    var fourthControlPoint = Offset(size.width * .2, size.height);
    var fourthEndPoint = Offset(0, size.height * .85);
    path.quadraticBezierTo(fourthControlPoint.dx, fourthControlPoint.dy,
        fourthEndPoint.dx, fourthEndPoint.dy);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper oldClipper) {
    return true;
  }
}
