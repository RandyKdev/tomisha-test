import 'package:flutter/material.dart';
import 'package:tomisha/colors.dart';
import 'package:tomisha/text_styles.dart';
import 'package:tomisha/widgets/button_widget.dart';

class BannerDesktopWidget extends StatelessWidget {
  final String imageLocation;
  final String title;
  final String ctaTitle;
  const BannerDesktopWidget({
    super.key,
    required this.imageLocation,
    required this.title,
    required this.ctaTitle,
  });

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipBehavior: Clip.hardEdge,
      clipper: _CustomClipperBannerDesktop(),
      child: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: [AppColors.bgBlue, AppColors.lightTeal],
            begin: FractionalOffset(-10, 0.0),
            end: FractionalOffset(1.0, 0.0),
            tileMode: TileMode.clamp,
          ),
        ),
        width: double.maxFinite,
        padding: const EdgeInsets.only(bottom: 20, top: 80, right: 20),
        child: Center(
          child: ConstrainedBox(
            constraints: const BoxConstraints(
              maxWidth: 1000,
            ),
            child: Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Text(
                          title,
                          style: AppTextStyles.boldStyle(
                            fontSize: 60,
                            color: AppColors.black,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),
                      const SizedBox(height: 40),
                      ButtonWidget(
                        title: ctaTitle,
                        onTap: () {},
                      ),
                    ],
                  ),
                ),
                const SizedBox(width: 30),
                Expanded(
                  child: CircleAvatar(
                    backgroundColor: Colors.white,
                    foregroundColor: Colors.white,
                    minRadius: 120,
                    maxRadius: 200,
                    child: ClipOval(
                      clipBehavior: Clip.hardEdge,
                      child: Image.asset(
                        imageLocation,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _CustomClipperBannerDesktop extends CustomClipper<Path> {
  @override
  getClip(Size size) {
    var path = Path();
    path.lineTo(0.0, size.height);
    path.lineTo(size.width * .2, size.height);
    var firstControlPoint = Offset(size.width * 7, size.height * .85);
    var firstEndPoint = Offset(size.width, size.height * .95);
    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy,
        firstEndPoint.dx, firstEndPoint.dy);
    path.lineTo(size.width, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper oldClipper) {
    return true;
  }
}
