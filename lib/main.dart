import 'package:flutter/material.dart';
import 'package:tomisha/screens/home_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Tomisha Jobs Test',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(
          seedColor: const Color(0xff3182CE),
          primary: const Color(0xff3182CE),
          secondary: const Color(0xff81E6D9),
          background: const Color(0xffFFFFFF),
          tertiary: const Color(0xff319795),
        ),
        useMaterial3: false,
        fontFamily: 'Lato',
      ),
      home: const HomeScreen(),
      debugShowCheckedModeBanner: false,
    );
  }
}
