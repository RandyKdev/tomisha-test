class TabItem {
  final String title;
  final String imageLocation;

  TabItem({
    required this.title,
    required this.imageLocation,
  });
}
