import 'package:flutter/material.dart';

class AppTextStyles {
  static TextStyle mediumStyle({
    double? fontSize,
    Color? color,
  }) {
    return TextStyle(
      fontWeight: FontWeight.w300,
      fontSize: fontSize,
      color: color,
    );
  }

  static TextStyle mediumItalicStyle({
    double? fontSize,
    Color? color,
  }) {
    return TextStyle(
      fontWeight: FontWeight.w300,
      fontStyle: FontStyle.italic,
      fontSize: fontSize,
      color: color,
    );
  }

  static TextStyle regularStyle({
    double? fontSize,
    Color? color,
  }) {
    return TextStyle(
      fontWeight: FontWeight.w400,
      fontSize: fontSize,
      color: color,
    );
  }

  static TextStyle regularItalicStyle({
    double? fontSize,
    Color? color,
  }) {
    return TextStyle(
      fontWeight: FontWeight.w400,
      fontStyle: FontStyle.italic,
      fontSize: fontSize,
      color: color,
    );
  }

  static TextStyle semiBoldStyle({
    double? fontSize,
    Color? color,
  }) {
    return TextStyle(
      fontWeight: FontWeight.w700,
      fontSize: fontSize,
      color: color,
    );
  }

  static TextStyle semiBoldItalicStyle({
    double? fontSize,
    Color? color,
  }) {
    return TextStyle(
      fontWeight: FontWeight.w700,
      fontStyle: FontStyle.italic,
      fontSize: fontSize,
      color: color,
    );
  }

  static TextStyle boldStyle({
    double? fontSize,
    Color? color,
  }) {
    return TextStyle(
      fontWeight: FontWeight.w900,
      fontSize: fontSize,
      color: color,
    );
  }

  static TextStyle boldItalicStyle({
    double? fontSize,
    Color? color,
  }) {
    return TextStyle(
      fontWeight: FontWeight.w900,
      fontStyle: FontStyle.italic,
      fontSize: fontSize,
      color: color,
    );
  }
}
