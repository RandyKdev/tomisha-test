import 'package:flutter/material.dart';

class AppColors {
  static const white = Color(0xffffffff);
  static const darkBlue = Color(0xff4A5568);
  static const veryLightBlue = Color(0xffF7FAFC);
  static const lightBlue = Color(0xff718096);
  static const lightTeal = Color(0xffE6FFFA);
  static const bgBlue = Color(0xffEBF4FF);
  static const black = Color(0xff2D3748);
  static const greyishBlue = Color(0xffCBD5E0);
  static const teal = Color(0xff81E6D9);
  static const green = Color(0xff319795);
  static const lightGrey = Color(0xff000029);
  static const blue = Color(0xff3182CE);
  static const grey = Color(0xff000033);
  static const darkGrey = Color(0xff707070);
}
