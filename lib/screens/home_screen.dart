import 'package:flutter/material.dart';
import 'package:tomisha/breakpoints.dart';
import 'package:tomisha/colors.dart';
import 'package:tomisha/models/tab_item.dart';
import 'package:tomisha/text_styles.dart';
import 'package:tomisha/widgets/banner_desktop_widget.dart';
import 'package:tomisha/widgets/banner_mobile_widget.dart';
import 'package:tomisha/widgets/button_widget.dart';
import 'package:tomisha/widgets/tab_bar_widget.dart';
import 'package:tomisha/widgets/tab_body_widget.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _currentIndex = 0;

  final title = 'Desing Job Website';
  final titleImageLocation = 'assets/images/agreement.png';
  final ctaTitle = 'Kostenlos Registrieren';
  final tabs = ['Arbeitnehmer', 'Arbeitgeber', 'Temporärbüro'];
  final bodyTitle = 'Drei einfache Schritte zu deinem neuen Job';

  final tabItems = [
    [
      TabItem(
        title: 'Erstellen dein Lebenslauf',
        imageLocation: 'assets/images/profile_data.png',
      ),
      TabItem(
        title: 'Erstellen dein Lebenslauf',
        imageLocation: 'assets/images/job_offers.png',
      ),
      TabItem(
        title: 'Mit nur einem Klick bewerben',
        imageLocation: 'assets/images/business_deal.png',
      ),
    ],
    [
      TabItem(
        title: 'Erstellen dein Lebenslauf',
        imageLocation: 'assets/images/profile_data.png',
      ),
      TabItem(
        title: 'Erstellen ein Jobinserat',
        imageLocation: 'assets/images/about_me.png',
      ),
      TabItem(
        title: 'Wähle deinen neuen Mitarbeiter aus',
        imageLocation: 'assets/images/swipe_profiles.png',
      ),
    ],
    [
      TabItem(
        title: 'Erstellen dein Lebenslauf',
        imageLocation: 'assets/images/profile_data.png',
      ),
      TabItem(
        title: 'Erhalte Vermittlungs- angebot von Arbeitgeber',
        imageLocation: 'assets/images/personal_file.png',
      ),
      TabItem(
        title: 'Vermittlung nach Provision oder Stundenlohn',
        imageLocation: 'assets/images/business_deal.png',
      ),
    ],
  ];

  Widget _buildMobileView() {
    return Column(
      children: [
        BannerMobileWidget(
          imageLocation: titleImageLocation,
          title: title,
        ),
        Material(
          elevation: 3,
          borderRadius: BorderRadius.circular(15),
          child: Container(
            decoration: BoxDecoration(
              color: AppColors.white,
              borderRadius: BorderRadius.circular(15),
            ),
            padding: const EdgeInsets.symmetric(vertical: 25),
            child: Column(
              children: [
                ButtonWidget(
                  title: ctaTitle,
                  onTap: () {},
                ),
                const Divider(),
                TabBarWidget(
                  onSelect: (index) {
                    setState(() {
                      _currentIndex = index;
                    });
                  },
                  currentIndex: _currentIndex,
                  tabs: tabs,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 20,
                    vertical: 20,
                  ),
                  child: Text(
                    bodyTitle,
                    style: AppTextStyles.mediumStyle(
                      fontSize: 18,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                TabBodyWidget(items: tabItems[_currentIndex]),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildDesktopView() {
    return Column(
      children: [
        BannerDesktopWidget(
          imageLocation: titleImageLocation,
          title: title,
          ctaTitle: ctaTitle,
        ),
        TabBarWidget(
          onSelect: (index) {
            setState(() {
              _currentIndex = index;
            });
          },
          currentIndex: _currentIndex,
          tabs: tabs,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 20,
            vertical: 20,
          ),
          child: Text(
            bodyTitle,
            style: AppTextStyles.mediumStyle(
              fontSize: 30,
            ),
            textAlign: TextAlign.center,
          ),
        ),
        TabBodyWidget(items: tabItems[_currentIndex]),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.sizeOf(context);

    return Scaffold(
      backgroundColor: AppBreakpoints.isMobile(size.width)
          ? AppColors.lightTeal
          : AppColors.white,
      appBar: AppBar(
        // clipBehavior: Clip.hardEdge,
        elevation: 3,
        backgroundColor: AppColors.white,
        scrolledUnderElevation: 3,
        actions: [
          TextButton(
            onPressed: () {},
            child: Text(
              'Login',
              style: AppTextStyles.semiBoldStyle(
                color: AppColors.green,
                fontSize: 14,
              ),
            ),
          ),
        ],
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(10),
            bottomRight: Radius.circular(10),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: AppBreakpoints.isMobile(size.width)
              ? _buildMobileView()
              : _buildDesktopView(),
        ),
      ),
    );
  }
}
