class AppBreakpoints {
  static const mobile = 650;

  static bool isMobile(double width) => width <= mobile;
  static bool isDesktop(double width) => !isMobile(width);
}
